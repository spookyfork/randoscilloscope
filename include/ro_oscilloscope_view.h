#pragma once

#include "vstgui/vstgui.h"
#include "pluginterfaces/base/ftypes.h"
#include "pluginterfaces/vst/vsttypes.h"

#include "ro_cids.h"

using namespace VSTGUI;

namespace RandOhm {
	class Oscilloscope : public CControl
	{
	public:

		enum Mode {
			LINE = 1,
			DOTS
		};

		Oscilloscope(const CRect& size);
		~Oscilloscope();

		void draw(CDrawContext* pContext) override;

		CLASS_METHODS(Oscilloscope, CControl);

		void update(float dt);
		bool attached(CView* parent);


		void setData(Steinberg::Vst::Sample32* data, Steinberg::int64 size);
		void setYData(Steinberg::Vst::Sample32* data, Steinberg::int64 size);
		void setSampleRate(double sampleRate);
		void setDataIndexPointer(int* i) { bufferIndexPtr = i; }
		void setFadeoutTime(float t);
		void setLineThreshold(float t) { lineThreshold = t; }
		void setLineAlpha(int t) { oscColor.alpha = t; }
		void setFlipX(bool v) { xSing = v ? -1.f : 1.f; }
		void setFlipY(bool v) { ySign = v ? -1.f : 1.f; }
		void setMode(Mode m) { mode = m; }

	protected:

		int moveTowards(int val, int target, int delta)
		{
			if (val == target) return val;
			int sign = (target - val) / abs(target- val);
			delta = abs(delta) * sign;
			val += delta;
			if (val != target && (target - val) / abs(target - val) != sign)
			{
				val = target;
			}
			return val;
		}

		float angle = 0.;

		Steinberg::Vst::Sample32* dataPtr = nullptr;
		Steinberg::Vst::Sample32* yPtr = nullptr;
		int* bufferIndexPtr = nullptr;
		Steinberg::int64 arraySize = 0;
		unsigned int index = 0;
		float sampleRate = 48000.f;
		float yScale = 1.f;
		float ySign = 1.f;
		float xSing = 1.f;


		// IMAGE 
		Mode mode = Mode::LINE;
		SharedPointer<VSTGUI::COffscreenContext> offscreen;
		VSTGUI::CColor backgroundColor;
		VSTGUI::CColor oscColor;
		float fadeTime = 0.1; //second
		float lineThreshold = 0.5;
		float dt = 0.0;
		float rDelta, gDelta, bDelta;
		CBitmap* bm = nullptr;
		CBitmapPixelAccess* bmpa = nullptr;

	};
}