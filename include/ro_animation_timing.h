#pragma once


#include "vstgui/vstgui.h"


using namespace VSTGUI;

namespace RandOhm {
	class RandAnimationTiming : public Animation::TimingFunctionBase
	{
	public:
		explicit RandAnimationTiming();
		~RandAnimationTiming();
		// Inherited via TimingFunctionBase
		float getPosition(uint32_t milliseconds) override;
		bool isDone(uint32_t milliseconds) override { return false; };
	protected:
		uint32_t lastTime = 0;

	};
}