#pragma once

#include "vstgui/vstgui.h"
using namespace VSTGUI;

namespace RandOhm {
	class RandAnimationTarget : public Animation::IAnimationTarget, public ReferenceCounted<int32_t>
	{
	public:
		// Inherited via IAnimationTarget
		void animationStart(CView* view, IdStringPtr name) override;
		void animationTick(CView* view, IdStringPtr name, float pos) override;
		void animationFinished(CView* view, IdStringPtr name, bool wasCanceled) override;

	protected:

	};
}