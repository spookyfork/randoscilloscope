#pragma once

#include "ro_cids.h"
#include "pluginterfaces/vst/vsttypes.h"

namespace RandOhm{
	class ParameterListener {
	public:
		virtual ~ParameterListener() noexcept = default;
		virtual void parameterChanged(Steinberg::Vst::ParamID tag, Steinberg::Vst::ParamValue newValue) = 0;
	};

}