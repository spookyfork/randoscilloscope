//------------------------------------------------------------------------
// Copyright(c) 2021 randOhm.
//------------------------------------------------------------------------

#pragma once

#include "pluginterfaces/base/funknown.h"
#include "pluginterfaces/vst/vsttypes.h"

namespace RandOhm {
//------------------------------------------------------------------------
static const Steinberg::FUID kRand_OscilloscopeProcessorUID (0x0296D47B, 0xE82E529F, 0xBF0C7A8B, 0x43B918F1);
static const Steinberg::FUID kRand_OscilloscopeControllerUID (0x0356C1D8, 0xEDEA5DA4, 0xA9F42E90, 0x1612DB3F);

#define Rand_OscilloscopeVST3Category "Fx"

//------------------------------------------------------------------------

#define MATH_PI 3.14159265358979323846264338327950288

#define RAND_OSC_FQ_MIN 10.0
#define RAND_OSC_FQ_MAX 10000.0
#define RAND_OSC_FQ_DEF 200.0

#define RAND_OSC_AMP_MIN 0.0
#define RAND_OSC_AMP_MAX 1.0
#define RAND_OSC_AMP_DEF 0.8

#define RAND_OSC_PHASE_MIN 0.0
#define RAND_OSC_PHASE_MAX 2 * MATH_PI
#define RAND_OSC_PHASE_DEF 0.0


enum ParameterID : Steinberg::Vst::ParamID {
	X_SELECTOR = 101,
	Y_SELECTOR = 102,

	LINES_DOTS = 202,

	FADEOUT_TIME = 301,
	LINE_THRESHOLD = 302,
	LINE_ALPHA = 303,
	FLIP_X = 304,
	FLIP_Y = 305,

	OSC1_TYPE = 10001,
	OSC1_AMP = 10002,
	OSC1_FREQ = 10003,
	OSC1_PHASE = 10004,

	OSC2_TYPE = 20001,
	OSC2_AMP = 20002,
	OSC2_FREQ = 20003,
	OSC2_PHASE = 20004,

	//BYPASS = 90001,
	MUTE = 90002
};

enum OscillatorType : int{
	SIN = 0,
	SAW,
	TRI,
	SQ,
	NCOUNT
};


#define RAND_OSC_TYPE_DEF OscillatorType::SAW

} // namespace RandOhm
