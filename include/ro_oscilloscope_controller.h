#pragma once

#include <mutex>          // std::mutex, std::lock

#include "vstgui/lib/iviewlistener.h"
#include "vstgui/uidescription/icontroller.h"

#include "pluginterfaces\vst\vsttypes.h"

#include "../include/ro_controller.h"
#include "../include/ro_oscilloscope_view.h"
#include "../include/ro_parameter_listener.h"

using namespace VSTGUI;


namespace RandOhm {
	template <typename ControllerType>
	class OscilloscopeController : public VSTGUI::IController, public VSTGUI::ViewListenerAdapter, public ParameterListener
	{
	public:

		explicit OscilloscopeController(ControllerType* parent) : mainController(parent) {
			if (mainController)
				mainController->addParameterListener(this);
		};

		~OscilloscopeController()
		{
			if (oscilloscopeView) oscilloscopeView->unregisterViewListener(this);
			oscilloscopeView = nullptr;
			if (mainController)
			{
				mainController->removeParameterListener(this);
				mainController->deleteSubController();
			}
		};

		// Inherited via IController
		void valueChanged(CControl* pControl) override
		{
			//...
		}

		CView* verifyView(CView* view, const UIAttributes& attributes, const IUIDescription* description) override
		{
			if (auto osc = dynamic_cast<Oscilloscope*>(view)) {
				oscilloscopeView = osc;
				oscilloscopeView->registerViewListener(this);
				updateView();
			}
			return view;
		}

		void viewSizeChanged(CView* view, const CRect& oldSize) override
		{
			//...
		}

		void viewAttached(CView* view) override
		{
			if (auto osc = dynamic_cast<Oscilloscope*>(view)) {
				oscilloscopeView = osc;
				updateView();
			}
		}

		void viewRemoved(CView* view) override
		{
			oscilloscopeView->unregisterViewListener(this);
			oscilloscopeView = nullptr;
		}

		void viewLostFocus(CView* view) override
		{

		}

		void viewTookFocus(CView* view) override
		{

		}

		void viewWillDelete(CView* view) override {
			oscilloscopeView->unregisterViewListener(this);
			oscilloscopeView = nullptr;
		}

		void controlBeginEdit(CControl* pControl) override
		{

		}

		void controlEndEdit(CControl* pControl) override
		{

		}

		void controlTagWillChange(CControl* pControl) override
		{

		}

		void controlTagDidChange(CControl* pControl) override
		{

		}

		// ------------------------------------


		void setData(Steinberg::Vst::Sample32* dataPtr, Steinberg::int64 size)
		{
			this->dataPtr = dataPtr;
			this->arraySize = size;
			updateView();
		}

		void setYData(Steinberg::Vst::Sample32* dataPtr, Steinberg::int64 size)
		{
			this->yPtr = dataPtr;
			this->arraySize = size;
			updateView();
		}

		void setSampleRate(double sampleRate)
		{
			this->sampleRate = sampleRate;
			updateView();
		}

		void updateView()
		{
			if (oscilloscopeView)
			{
				oscilloscopeView->setSampleRate(sampleRate);
				oscilloscopeView->setData(dataPtr, arraySize);
				oscilloscopeView->setYData(yPtr, arraySize);
				oscilloscopeView->setDataIndexPointer(bufferIndexPtr);
			}
		}

		void setFedeoutTime(float t) {
			if (oscilloscopeView) oscilloscopeView->setFadeoutTime(t);
		}

		void setLineThreshold(float t) {
			if (oscilloscopeView) oscilloscopeView->setLineThreshold(t);
		}

		void setLineAlpha(int t) {
			if (oscilloscopeView) oscilloscopeView->setLineAlpha(t);
		}

		void setDataIndexPointer(int* i) { bufferIndexPtr = i; }

		// Inherited via ParameterListener
		void parameterChanged(Steinberg::Vst::ParamID tag, Steinberg::Vst::ParamValue newValue) override
		{
			switch (tag)
			{
			case ParameterID::FADEOUT_TIME:
				setFedeoutTime(newValue);
				break;
			case ParameterID::LINE_THRESHOLD:
				setLineThreshold(newValue);
				break;
			case ParameterID::LINE_ALPHA:
				setLineAlpha(newValue);
				break;
			case ParameterID::FLIP_X:
				if (oscilloscopeView) oscilloscopeView->setFlipX(newValue > 0.5);
				break;
			case ParameterID::FLIP_Y:
				if (oscilloscopeView) oscilloscopeView->setFlipY(newValue > 0.5);
				break;
			case ParameterID::LINES_DOTS:
				if (oscilloscopeView) oscilloscopeView->setMode(newValue > 0.5 ? Oscilloscope::Mode::DOTS : Oscilloscope::Mode::LINE);
				break;
			default:
				break;
			}
		}

	protected:
		double sampleRate = 48000.0;
		Steinberg::Vst::Sample32* dataPtr = nullptr;
		Steinberg::Vst::Sample32* yPtr = nullptr;
		int* bufferIndexPtr = nullptr;
		Steinberg::int64 arraySize = 0;

		Oscilloscope* oscilloscopeView = nullptr;
		ControllerType* mainController = nullptr;




	};
}