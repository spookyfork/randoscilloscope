//------------------------------------------------------------------------
// Copyright(c) 2021 randOhm.
//------------------------------------------------------------------------

#pragma once

#include "vstgui/plugin-bindings/vst3editor.h"
#include "public.sdk/source/vst/vsteditcontroller.h"

#include "../include/ro_oscilloscope_controller.h"
#include "../include/ro_parameter_listener.h"

namespace RandOhm {

//------------------------------------------------------------------------
//  Rand_OscilloscopeController
//------------------------------------------------------------------------
class Rand_OscilloscopeController : public Steinberg::Vst::EditControllerEx1, public VSTGUI::VST3EditorDelegate
{
public:
//------------------------------------------------------------------------
	Rand_OscilloscopeController () = default;
	~Rand_OscilloscopeController () SMTG_OVERRIDE = default;

    // Create function
	static Steinberg::FUnknown* createInstance (void* /*context*/)
	{
		return (Steinberg::Vst::IEditController*)new Rand_OscilloscopeController;
	}

	// IPluginBase
	Steinberg::tresult PLUGIN_API initialize (Steinberg::FUnknown* context) SMTG_OVERRIDE;
	Steinberg::tresult PLUGIN_API terminate () SMTG_OVERRIDE;

	// EditController
	Steinberg::tresult PLUGIN_API setComponentState (Steinberg::IBStream* state) SMTG_OVERRIDE;
	Steinberg::IPlugView* PLUGIN_API createView (Steinberg::FIDString name) SMTG_OVERRIDE;
	Steinberg::tresult PLUGIN_API setState (Steinberg::IBStream* state) SMTG_OVERRIDE;
	Steinberg::tresult PLUGIN_API getState (Steinberg::IBStream* state) SMTG_OVERRIDE;
	Steinberg::tresult PLUGIN_API setParamNormalized (Steinberg::Vst::ParamID tag,
                                                      Steinberg::Vst::ParamValue value) SMTG_OVERRIDE;
	Steinberg::tresult PLUGIN_API getParamStringByValue (Steinberg::Vst::ParamID tag,
                                                         Steinberg::Vst::ParamValue valueNormalized,
                                                         Steinberg::Vst::String128 string) SMTG_OVERRIDE;
	Steinberg::tresult PLUGIN_API getParamValueByString (Steinberg::Vst::ParamID tag,
                                                         Steinberg::Vst::TChar* string,
                                                         Steinberg::Vst::ParamValue& valueNormalized) SMTG_OVERRIDE;

	

	// VSTGUI::VST3EditorDelegate
	VSTGUI::IController* createSubController(VSTGUI::UTF8StringPtr name, const VSTGUI::IUIDescription* description, VSTGUI::VST3Editor* editor) SMTG_OVERRIDE;
	void willClose(VST3Editor* editor);
	void deleteSubController() { if (subController) subController = nullptr; }

	/** Test of a communication channel between controller and component */
	Steinberg::tresult receiveText(const char* text) SMTG_OVERRIDE;

	/** We want to receive message. */
	Steinberg::tresult PLUGIN_API notify(Steinberg::Vst::IMessage* message) SMTG_OVERRIDE;

	void addParameterListener(ParameterListener *listener);
	void removeParameterListener(ParameterListener* listener);

	void updateSubController();

	//---Interface---------
	DEFINE_INTERFACES
		// Here you can add more supported VST3 interfaces
		// DEF_INTERFACE (Vst::IXXX)
	END_DEFINE_INTERFACES (EditController)
    DELEGATE_REFCOUNT (EditController)

//------------------------------------------------------------------------
protected:

	void fireParameterChanged(Steinberg::Vst::ParamID tag, Steinberg::Vst::ParamValue value);

	double sampleRate = 0.f;
	OscilloscopeController<Rand_OscilloscopeController> *subController = nullptr;
	Steinberg::int64 arraySize = 0;
	Steinberg::Vst::Sample32* dataPtr = nullptr;
	Steinberg::Vst::Sample32* yPtr = nullptr;
	int* bufferIndexPtr = nullptr;

	//VSTGUI has its own list class for listeners that we can use
	VSTGUI::DispatchList<ParameterListener*> parameterListeners;
};

//------------------------------------------------------------------------
} // namespace RandOhm
