//------------------------------------------------------------------------
// Copyright(c) 2021 randOhm.
//------------------------------------------------------------------------

#pragma once

#include "public.sdk/source/vst/vstaudioeffect.h"
#include "ro_cids.h"

namespace RandOhm {

struct OscillatorParams {
	OscillatorType type = RAND_OSC_TYPE_DEF;
	float amp = RAND_OSC_AMP_DEF;
	float freq = RAND_OSC_FQ_DEF;
	float phase = RAND_OSC_PHASE_DEF; //raw phase value
	double index = 0.; //internal index for calculating next sample
	double phaseInc = 0.0; //index jump for phase 
};


//------------------------------------------------------------------------
//  Rand_OscilloscopeProcessor
//------------------------------------------------------------------------
class Rand_OscilloscopeProcessor : public Steinberg::Vst::AudioEffect
{
public:
	Rand_OscilloscopeProcessor ();
	~Rand_OscilloscopeProcessor () SMTG_OVERRIDE;

    // Create function
	static Steinberg::FUnknown* createInstance (void* /*context*/) 
	{ 
		return (Steinberg::Vst::IAudioProcessor*)new Rand_OscilloscopeProcessor; 
	}

	//--- ---------------------------------------------------------------------
	// AudioEffect overrides:
	//--- ---------------------------------------------------------------------

	/** Called at first after constructor */
	Steinberg::tresult PLUGIN_API initialize (Steinberg::FUnknown* context) SMTG_OVERRIDE;
	
	/** Called at the end before destructor */
	Steinberg::tresult PLUGIN_API terminate () SMTG_OVERRIDE;
	
	/** Switch the Plug-in on/off */
	Steinberg::tresult PLUGIN_API setActive (Steinberg::TBool state) SMTG_OVERRIDE;

	/** Will be called before any process call */
	Steinberg::tresult PLUGIN_API setupProcessing (Steinberg::Vst::ProcessSetup& newSetup) SMTG_OVERRIDE;
	
	/** Asks if a given sample size is supported see SymbolicSampleSizes. */
	Steinberg::tresult PLUGIN_API canProcessSampleSize (Steinberg::int32 symbolicSampleSize) SMTG_OVERRIDE;

	/** Here we go...the process call */
	Steinberg::tresult PLUGIN_API process (Steinberg::Vst::ProcessData& data) SMTG_OVERRIDE;
		
	/** For persistence */
	Steinberg::tresult PLUGIN_API setState (Steinberg::IBStream* state) SMTG_OVERRIDE;
	Steinberg::tresult PLUGIN_API getState (Steinberg::IBStream* state) SMTG_OVERRIDE;


	/** Test of a communication channel between controller and component */
	Steinberg::tresult receiveText(const char* text) SMTG_OVERRIDE;

	/** We want to receive message. */
	Steinberg::tresult PLUGIN_API notify(Steinberg::Vst::IMessage* message) SMTG_OVERRIDE;

//------------------------------------------------------------------------
protected:
	void generateSinSamples(double sampleRate);
	void generateSawSamples(double sampleRate);
	void generateTriangeSamples(double sampleRate);
	void generateSquareSamples(double sampleRate);
	void generateOscilloscopeBuffers(double sampleRate);
	Steinberg::Vst::Sample32 getSample(OscillatorParams& osc);
	void nextIndex(OscillatorParams& osc);

	double sampleRate = 0.0;

	//Oscillators
	Steinberg::Vst::Sample32* sinSamples = nullptr;
	Steinberg::Vst::Sample32* squareSamples = nullptr;
	Steinberg::Vst::Sample32* triangleSamples = nullptr;
	Steinberg::Vst::Sample32* sawSamples = nullptr;
	int arraySize = 0;


	int bufferIndex = 0;
	Steinberg::Vst::Sample32* xBuffer = nullptr;
	Steinberg::Vst::Sample32* yBuffer = nullptr;

	bool channel1Input = true;
	bool channel2Input = true;

	bool pMute = false;

	OscillatorParams osc1;
	OscillatorParams osc2;
	Steinberg::uint64 currentOscSample = 0;
};

//------------------------------------------------------------------------
} // namespace RandOhm
