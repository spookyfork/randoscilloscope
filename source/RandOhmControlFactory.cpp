#pragma once

#include "vstgui/vstgui.h"
#include "vstgui/vstgui_uidescription.h"
#include "vstgui/uidescription/detail/uiviewcreatorattributes.h"

#include "../include/ro_oscilloscope_view.h"

namespace VSTGUI {
	class RandOhmControlFactory : public ViewCreatorAdapter
	{
	public:
		//register this class with the view factory
		RandOhmControlFactory() { UIViewFactory::registerViewCreator(*this); }

		IdStringPtr getViewName() const { return "RandOhm Oscilloscope"; }
		IdStringPtr getBaseViewName() const { return UIViewCreator::kCControl; }

		CView* create(const UIAttributes& attributes, const IUIDescription* description) const
		{
			CRect size(CPoint(0, 0), CPoint(200, 200));
			return new RandOhm::Oscilloscope(size);
		}
	};

	RandOhmControlFactory __gMyControlFactory;
}