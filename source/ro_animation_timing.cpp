#include "../include/ro_animation_timing.h"

namespace RandOhm {
	RandAnimationTiming::RandAnimationTiming() : TimingFunctionBase(0)
	{
	}

	RandAnimationTiming::~RandAnimationTiming()
	{
	}

	float RandAnimationTiming::getPosition(uint32_t milliseconds)
	{
		float dt = (milliseconds - lastTime) / 1000.f;
		lastTime = milliseconds;
		return dt;
	}
}