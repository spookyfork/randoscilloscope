//------------------------------------------------------------------------
// Copyright(c) 2021 randOhm.
//------------------------------------------------------------------------

#include <algorithm>
#include "../include/ro_controller.h"
#include "../include/ro_cids.h"
#include "base/source/fstreamer.h"
#include "vstgui/plugin-bindings/vst3editor.h"

using namespace Steinberg;

namespace RandOhm {

//------------------------------------------------------------------------
// Rand_OscilloscopeController Implementation
//------------------------------------------------------------------------
tresult PLUGIN_API Rand_OscilloscopeController::initialize (FUnknown* context)
{
	// Here the Plug-in will be instanciated

	//---do not forget to call parent ------
	tresult result = EditControllerEx1::initialize (context);
	if (result != kResultOk)
	{
		return result;
	}

	// Here you could register some parameters
	// 
	// TODO: Check if there is a better way to store settings that are not related to the processing of audio signal
	// Things like Flip X, Flip Y, and other display settings 
	parameters.addParameter(STR16("X Input"), nullptr, 1, 0, 0, ParameterID::X_SELECTOR);
	parameters.addParameter(STR16("Y Input"), nullptr, 1, 0, 0, ParameterID::Y_SELECTOR);
	parameters.addParameter(STR16("Flip X"), nullptr, 1, 0, 0, ParameterID::FLIP_X);
	parameters.addParameter(STR16("Flip Y"), nullptr, 1, 0, 0, ParameterID::FLIP_Y);
	parameters.addParameter(STR16("Line/Dots"), nullptr, 1, 0, 0, ParameterID::LINES_DOTS);

	//This is probably unnecessary (this way)
	Vst::StringListParameter* sl1 = new Vst::StringListParameter(STR16("OSC1 Type"), ParameterID::OSC1_TYPE);
	sl1->appendString(STR16("SIN"));
	sl1->appendString(STR16("SAW"));
	sl1->appendString(STR16("TRI"));
	sl1->appendString(STR16("SQ"));
	sl1->setNormalized(RAND_OSC_TYPE_DEF / (double)OscillatorType::NCOUNT);
	parameters.addParameter(sl1);

	//This is probably unnecessary
	Vst::StringListParameter* sl2 = new Vst::StringListParameter(STR16("OSC2 Type"), ParameterID::OSC2_TYPE);
	sl2->appendString(STR16("SIN"));
	sl2->appendString(STR16("SAW"));
	sl2->appendString(STR16("TRI"));
	sl2->appendString(STR16("SQ"));
	sl2->setNormalized(RAND_OSC_TYPE_DEF / (double)OscillatorType::NCOUNT);
	parameters.addParameter(sl2);

	Vst::RangeParameter *fq1 = new Vst::RangeParameter(STR16("OSC1 Freq"), ParameterID::OSC1_FREQ, STR16("Hz"), RAND_OSC_FQ_MIN, RAND_OSC_FQ_MAX, RAND_OSC_FQ_DEF);
	Vst::RangeParameter *fq2 = new Vst::RangeParameter(STR16("OSC2 Freq"), ParameterID::OSC2_FREQ, STR16("Hz"), RAND_OSC_FQ_MIN, RAND_OSC_FQ_MAX, RAND_OSC_FQ_DEF);
	fq1->setPrecision(2);
	fq2->setPrecision(2);
	parameters.addParameter(fq1);
	parameters.addParameter(fq2);

	Vst::RangeParameter* amp1 = new Vst::RangeParameter(STR16("OSC1 Amp"), ParameterID::OSC1_AMP, nullptr, RAND_OSC_AMP_MIN, RAND_OSC_AMP_MAX, RAND_OSC_AMP_DEF);
	Vst::RangeParameter* amp2 = new Vst::RangeParameter(STR16("OSC2 Amp"), ParameterID::OSC2_AMP, nullptr, RAND_OSC_AMP_MIN, RAND_OSC_AMP_MAX, RAND_OSC_AMP_DEF);
	parameters.addParameter(amp1);
	parameters.addParameter(amp2);

	Vst::RangeParameter* ph1 = new Vst::RangeParameter(STR16("OSC1 Phase"), ParameterID::OSC1_PHASE, STR16("Rad"), RAND_OSC_PHASE_MIN, RAND_OSC_PHASE_MAX, RAND_OSC_PHASE_DEF);
	Vst::RangeParameter* ph2 = new Vst::RangeParameter(STR16("OSC2 Phase"), ParameterID::OSC2_PHASE, STR16("Rad"), RAND_OSC_PHASE_MIN, RAND_OSC_PHASE_MAX, RAND_OSC_PHASE_DEF);
	parameters.addParameter(ph1);
	parameters.addParameter(ph2);

	parameters.addParameter(STR16("Mute"), nullptr, 1, 0.0, 0, ParameterID::MUTE);
	parameters.addParameter(STR16("Fade Out Time"), STR16("s"), 0, 0.2, Vst::ParameterInfo::kCanAutomate, ParameterID::FADEOUT_TIME);

	Vst::RangeParameter* lt = new Vst::RangeParameter(STR16("Line Threshold"), ParameterID::LINE_THRESHOLD, nullptr, 0, 2, 0.5);
	parameters.addParameter(lt);
	Vst::RangeParameter* la = new Vst::RangeParameter(STR16("Line Alpha"), ParameterID::LINE_ALPHA, nullptr, 0, 255, 100, 255);
	parameters.addParameter(la);
	return result;
}

//------------------------------------------------------------------------
tresult PLUGIN_API Rand_OscilloscopeController::terminate ()
{
	// Here the Plug-in will be de-instanciated, last possibility to remove some memory!
	
	//---do not forget to call parent ------
	return EditControllerEx1::terminate ();
}

//------------------------------------------------------------------------
tresult PLUGIN_API Rand_OscilloscopeController::setComponentState (IBStream* state)
{
	// Here you get the state of the component (Processor part)
	if (!state)
		return kResultFalse;

	return kResultOk;
}

//------------------------------------------------------------------------
tresult PLUGIN_API Rand_OscilloscopeController::setState (IBStream* state)
{
	// Here you get the state of the controller
	IBStreamer streamer(state, kLittleEndian);
	return kResultTrue;
}

//------------------------------------------------------------------------
tresult PLUGIN_API Rand_OscilloscopeController::getState (IBStream* state)
{
	// Here you are asked to deliver the state of the controller (if needed)
	// Note: the real state of your plug-in is saved in the processor
	IBStreamer streamer(state, kLittleEndian);

	return kResultTrue;
}

//------------------------------------------------------------------------
IPlugView* PLUGIN_API Rand_OscilloscopeController::createView (FIDString name)
{
	// Here the Host wants to open your editor (if you have one)
	if (FIDStringsEqual (name, Vst::ViewType::kEditor))
	{
		// create your editor here and return a IPlugView ptr of it
		auto* view = new VSTGUI::VST3Editor (this, "view", "ro_editor.uidesc");
		return view;
	}
	return nullptr;
}

//------------------------------------------------------------------------
tresult PLUGIN_API Rand_OscilloscopeController::setParamNormalized (Vst::ParamID tag, Vst::ParamValue value)
{
	fireParameterChanged(tag, value);
	// called by host to update your parameters
	tresult result = EditControllerEx1::setParamNormalized (tag, value);

	return result;
}

//------------------------------------------------------------------------
tresult PLUGIN_API Rand_OscilloscopeController::getParamStringByValue (Vst::ParamID tag, Vst::ParamValue valueNormalized, Vst::String128 string)
{
	// called by host to get a string for given normalized value of a specific parameter
	// (without having to set the value!)
	return EditControllerEx1::getParamStringByValue (tag, valueNormalized, string);
}

//------------------------------------------------------------------------
tresult PLUGIN_API Rand_OscilloscopeController::getParamValueByString (Vst::ParamID tag, Vst::TChar* string, Vst::ParamValue& valueNormalized)
{
	// called by host to get a normalized value from a string representation of a specific parameter
	// (without having to set the value!)
	return EditControllerEx1::getParamValueByString (tag, string, valueNormalized);
}


VSTGUI::IController* Rand_OscilloscopeController::createSubController(VSTGUI::UTF8StringPtr name, const VSTGUI::IUIDescription* description, VSTGUI::VST3Editor* editor)
{
	if (strcmp(name, "OscilloscopeController") == 0) {
		subController = new OscilloscopeController<Rand_OscilloscopeController>(this);
		updateSubController(); //subController is created after we get the message
		return subController;
	}
	return nullptr;
}

void Rand_OscilloscopeController::willClose(VSTGUI::VST3Editor* editor)
{
	subController = nullptr;
}

Steinberg::tresult Rand_OscilloscopeController::receiveText(const char* text)
{

	return kResultOk;
}

Steinberg::tresult PLUGIN_API Rand_OscilloscopeController::notify(Steinberg::Vst::IMessage* message)
{
	//get samples from the processor and add them to the view
	//we do this by passing them to the view controller that then passes them to the view
	if (strcmp(message->getMessageID(), "Setup") == 0) {
		//Steinberg::int64 ptr;
		const void* ptr;
		uint32 size = 0;
		int64 tmp = 0;
		//Since in the processor we pass the address of the pointer that points to buffer
		//we need to take the received data and cast it to a pointer to a pointer
		//and then take the value stored in the first pointer
		if (message->getAttributes()->getBinary("DataPointer", ptr, size) == kResultOk)
		{
			dataPtr = *((Steinberg::Vst::Sample32**)ptr);
		}
		if (message->getAttributes()->getBinary("yPointer", ptr, size) == kResultOk)
		{
			yPtr = *((Steinberg::Vst::Sample32**)ptr);
		}
		if (message->getAttributes()->getInt("BufferIndexPointer", tmp) == kResultOk)
		{
			bufferIndexPtr = (int*)tmp;
		}
		if (message->getAttributes()->getFloat("SampleRate", sampleRate) == kResultOk)
		{
		}
		if (message->getAttributes()->getInt("ArraySize", arraySize) == kResultOk)
		{
		}
		return kResultOk;
	}

	//Pass to ComponentBase to check if this is a text message -- processing is int receiveText()
	return ComponentBase::notify(message);
}

void Rand_OscilloscopeController::addParameterListener(ParameterListener* listener)
{
	parameterListeners.add(listener);
}

void Rand_OscilloscopeController::removeParameterListener(ParameterListener* listener)
{
	parameterListeners.remove(listener);
}

void Rand_OscilloscopeController::updateSubController()
{
	if (subController)
	{
		subController->setSampleRate(sampleRate);
		subController->setData(dataPtr, arraySize);
		subController->setYData(yPtr, arraySize);
		subController->setDataIndexPointer(bufferIndexPtr);
	}
}

void Rand_OscilloscopeController::fireParameterChanged(Vst::ParamID tag, Steinberg::Vst::ParamValue value)
{
	parameterListeners.forEach([&](ParameterListener* l) {
		l->parameterChanged(tag, parameters.getParameter(tag)->toPlain(value));
	});
}


//------------------------------------------------------------------------
} // namespace RandOhm
