#include "../include/ro_oscilloscope_view.h"

#include "../include/ro_animation_target.h"
#include "../include/ro_animation_timing.h"

#include <algorithm>

namespace RandOhm {
	Oscilloscope::Oscilloscope(const CRect& size) : VSTGUI::CControl(size) 
	{
		backgroundColor = CColor(25, 25, 25, 255);
		oscColor = CColor(0, 255, 0, 100);

	}

	Oscilloscope::~Oscilloscope()
	{
		removeAllAnimations();
	}

	void Oscilloscope::draw(CDrawContext* pContext)
	{
		// Move the default (0, 0) point of the drawing operations to the center
		auto viewPos = offscreen->getSurfaceRect().getCenter();
		bm = offscreen->getBitmap();
		bmpa = CBitmapPixelAccess::create(bm); //do not call this between beginDraw() and endDraw()
		if (bmpa)
		{
			bmpa->setPosition(0, 0);
			float rDt = rDelta * dt;
			float gDt = gDelta * dt;
			float bDt = bDelta * dt;
			//TODO: We need to make sure those values are at least above 1.0 or below -1.0
			//Otherwise, if the fadeTime is too long and the difference between background color and line color is too big
			//those values will become 0 before the pixels fully reach target color
			do 
			{
				CColor col;
				bmpa->getColor(col);
				col.red = moveTowards(col.red, 25, rDt);
				col.green = moveTowards(col.green, 25, gDt);
				col.blue = moveTowards(col.blue, 25, bDt);
				bmpa->setColor(col);
			} while (bmpa->operator++());
		}

		//Need to forget to make changes to the bitmap
		bmpa->forget();

		offscreen->beginDraw();
		offscreen->setFrameColor(oscColor); //Color of lines
		CDrawContext::Transform t(*offscreen, CGraphicsTransform().translate(viewPos)); //TODO: test if this needs to be called every time
		
		/********** Draw spinning line (updated via animator calling update() method) **********/
		//double radius = getViewSize().getHeight() * 0.4;
		//CPoint first(radius * cosf(angle), radius * sinf(angle));
		//CPoint second(0.f, 0.f);
		//pContext->drawLine(first, second);

		/********** Custom text drawing example **********/
		//pContext->setFontColor(CColor(255, 0, 0, 255));
		//pContext->setFont(VSTGUI::kNormalFont, 20);
		//pContext->drawString("TEST", CPoint(0, 0));
		//pContext->drawString("TEST", CRect(10, 10, 200, 200)); // default alignment is kCenterText, so if the rect is too big it will be rendered out of view


		double height = getViewSize().getHeight();
		double width = getViewSize().getWidth();
		CPoint lastPoint(0.0, 0.0);

		if (dataPtr)
		{
			//"Screen" refreshes 60 times a second according to documentation on how animators work 
			// This means we need to get all the samples from the last 1/60 of the second and display them
			//this could lead to loss of data, but whatever
			//this becomes an issue if the Y freq is lower than 60 and the image becomes fragmented because we don't draw the entire waveform
			/*
				Solution: 
					Store the image in a raster
					Before we draw we darken the image with some ratio (linearly)
					Every frame we draw the last 1/60 of a second worth of samples
				This should simulate the crt somewhat and give me control over how fast

				Problem:
					Because we draw 1/60 we might have X or Y coords jump from 1.0 to 0.0 (like Saw wave)
					which causes lines to be drawn across the screen. This would not be visible in a real oscilloscope
				Solution:
					adjust the width of the line based on the distance between samples 

				Problem:
					Image looks much noisier that the one shown on the real oscilloscope
					For instance the re are clear letters shown in the videos "Thank You" etc.
					but they are only berly legible in the program
				Possible solution:
					That is probably a Compression issue make sure the files are flac or wav

				TODO: It might be a good idea to affect the alpha of the lines based on their distance to the bufferIndexPtr (newest sample)
					Make the older samples lower alpha.
				TODO: Maybe draw the sample points themselves bigger/brighter or add and option
					With a linestyle or by drawing points/circles
			*/
			int points = dt * sampleRate; //Get the samples since the last screen refresh
			int firstIndex = *bufferIndexPtr - points;
			if (firstIndex < 0) firstIndex += arraySize;
			//need to change the sign of Y coord to have the regular euler coor sys(x goes up to the right side, y grows upwards)
			lastPoint = CPoint((dataPtr[firstIndex]), (yPtr[firstIndex]));
			firstIndex = (firstIndex + 1) % arraySize;
			if (firstIndex == arraySize) firstIndex = 0;
			float lineWidth = 1.0;
			for (int i = firstIndex; i != *bufferIndexPtr;) {
				//X is horizontal Y is Vertical
				CPoint currentPoint((dataPtr[i]), (yPtr[i]));
				if (mode == Mode::LINE)
				{
					lineWidth = (lastPoint.x - currentPoint.x) * (lastPoint.x - currentPoint.x) + (lastPoint.y - currentPoint.y) * (lastPoint.y - currentPoint.y);
					lineWidth = sqrtf(lineWidth);
					if (lineWidth > lineThreshold)
						lineWidth = 0;
					else
						lineWidth = 2 - 2 / lineThreshold * lineWidth;
					offscreen->setLineWidth(lineWidth);

					CPoint scaledLast(lastPoint);
					CPoint scaledCurrent(currentPoint);
					scaledLast.x *= xSing * width / 2;
					scaledLast.y *= ySign * height / 2;
					scaledCurrent.x *= xSing * width / 2;
					scaledCurrent.y *= ySign * height / 2;
					offscreen->drawLine(scaledLast, scaledCurrent);
				}
				else
				{
					CPoint scaledCurrent(currentPoint);
					scaledCurrent.x *= xSing * width / 2;
					scaledCurrent.y *= ySign * height / 2;
					offscreen->drawPoint(scaledCurrent, oscColor);
				}
				lastPoint = currentPoint;

				i++;
				if (i == arraySize) i = 0;
			}
		}
		
		offscreen->endDraw();
		//Draw the image onto the screen 
		offscreen->copyFrom(pContext, getViewSize());
		setDirty(false);
	}

	void Oscilloscope::update(float dt)
	{
		this->dt = dt;
		invalid();
	}

	bool Oscilloscope::attached(CView* parent)
	{
		//Animations are controlled by frame so we need to make sure our view is attached to add an animation
		CControl::attached(parent);
		offscreen = COffscreenContext::create(getViewSize().getSize());
		offscreen->setDrawMode(CDrawModeFlags::kAntiAliasing);
		offscreen->beginDraw();
		offscreen->setFillColor(backgroundColor);
		offscreen->drawRect(offscreen->getSurfaceRect(), kDrawFilled);
		offscreen->endDraw();

		addAnimation("RotatingAnimation", new RandAnimationTarget(), new RandAnimationTiming());
		return true;
	}

	void Oscilloscope::setData(Steinberg::Vst::Sample32* data, Steinberg::int64 size)
	{
		dataPtr = data;
		arraySize = size;
	}

	void Oscilloscope::setYData(Steinberg::Vst::Sample32* data, Steinberg::int64 size)
	{
		yPtr = data;
		//XXX: we assume the size is the same as X data
	}

	void Oscilloscope::setSampleRate(double sampleRate)
	{
		this->sampleRate = sampleRate;
	}

	void Oscilloscope::setFadeoutTime(float t)
	{ 
		fadeTime = t; 
		rDelta = (backgroundColor.red - oscColor.red) / fadeTime;
		gDelta = (backgroundColor.green - oscColor.green) / fadeTime;
		bDelta = (backgroundColor.blue - oscColor.blue) / fadeTime;
	}


}