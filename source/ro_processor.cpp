//------------------------------------------------------------------------
// Copyright(c) 2021 randOhm.
//------------------------------------------------------------------------

#include "../include/ro_processor.h"
#include "../include/ro_cids.h"

#include "base/source/fstreamer.h"
#include "pluginterfaces/vst/ivstparameterchanges.h"
#include "public.sdk/source/vst/vstaudioprocessoralgo.h"

using namespace Steinberg;

namespace RandOhm {
	//------------------------------------------------------------------------
	// Rand_OscilloscopeProcessor
	//------------------------------------------------------------------------
	Rand_OscilloscopeProcessor::Rand_OscilloscopeProcessor()
	{
		//--- set the wanted controller for our processor
		setControllerClass(kRand_OscilloscopeControllerUID);
	}

	//------------------------------------------------------------------------
	Rand_OscilloscopeProcessor::~Rand_OscilloscopeProcessor()
	{
		if (yBuffer) delete[] yBuffer;
		if (xBuffer) delete[] xBuffer;
		if (sinSamples) delete[] sinSamples;
		if (squareSamples) delete[] squareSamples;
		if (sawSamples) delete[] sawSamples;
		if (triangleSamples) delete[] triangleSamples;
	}

	//------------------------------------------------------------------------


	//------------------------------------------------------------------------
	tresult PLUGIN_API Rand_OscilloscopeProcessor::initialize(FUnknown* context)
	{
		// Here the Plug-in will be instanciated

		//---always initialize the parent-------
		tresult result = AudioEffect::initialize(context);
		// if everything Ok, continue
		if (result != kResultOk)
		{
			return result;
		}

		//--- create Audio IO ------
		addAudioInput(STR16("Stereo In"), Steinberg::Vst::SpeakerArr::kStereo);
		addAudioOutput(STR16("Stereo Out"), Steinberg::Vst::SpeakerArr::kStereo);
		/* If you don't need an event bus, you can remove the next line */
		addEventInput(STR16("Event In"), 1);

		return kResultOk;
	}

	//------------------------------------------------------------------------
	tresult PLUGIN_API Rand_OscilloscopeProcessor::terminate()
	{
		// Here the Plug-in will be de-instanciated, last possibility to remove some memory!
		if (yBuffer) delete[] yBuffer;
		if (xBuffer) delete[] xBuffer;
		if (sinSamples) delete[] sinSamples;
		if (squareSamples) delete[] squareSamples;
		if (sawSamples) delete[] sawSamples;
		if (triangleSamples) delete[] triangleSamples;
		yBuffer = xBuffer = sinSamples = squareSamples = sawSamples = triangleSamples = nullptr;

		//---do not forget to call parent ------
		return AudioEffect::terminate();
	}

	//------------------------------------------------------------------------
	tresult PLUGIN_API Rand_OscilloscopeProcessor::setActive(TBool state)
	{
		//--- called when the Plug-in is enable/disable (On/Off) -----
		return AudioEffect::setActive(state);
	}

	//------------------------------------------------------------------------
	tresult PLUGIN_API Rand_OscilloscopeProcessor::process(Vst::ProcessData& data)
	{
		//--- First : Read inputs parameter changes-----------
		//Width of the window should be a parameter hidden from the user
		//set from the controller when the window size change or when it's created


		if (data.inputParameterChanges)
		{
			int32 numParamsChanged = data.inputParameterChanges->getParameterCount();
			for (int32 index = 0; index < numParamsChanged; index++)
			{
				if (auto* paramQueue = data.inputParameterChanges->getParameterData(index))
				{
					Vst::ParamValue value;
					int32 sampleOffset;
					int32 numPoints = paramQueue->getPointCount();
					int discreteValue;
					if (paramQueue->getPoint(numPoints - 1, sampleOffset, value) == kResultTrue)
					{
						switch (paramQueue->getParameterId())
						{
						case ParameterID::X_SELECTOR:
							//can be only 0.0 or 1.0
							channel1Input = value < 0.5;
							break;
						case ParameterID::Y_SELECTOR:
							channel2Input = value < 0.5;
							break;
						case ParameterID::OSC1_TYPE:
							discreteValue = std::min((int)OscillatorType::NCOUNT - 1, (int)(value * (OscillatorType::NCOUNT)));
							osc1.type = (OscillatorType)discreteValue;
							break;
						case ParameterID::OSC1_AMP:
							osc1.amp = value;
							break;
						case ParameterID::OSC1_FREQ:
							osc1.freq = value * (RAND_OSC_FQ_MAX - RAND_OSC_FQ_MIN) + RAND_OSC_FQ_MIN;
							//Recalculate index value for the oscillator
							osc1.index = osc1.freq * currentOscSample;
							while (osc1.index >= sampleRate) osc1.index -= sampleRate;
							break;
						case ParameterID::OSC1_PHASE:
							osc1.phase = value * (RAND_OSC_PHASE_MAX - RAND_OSC_PHASE_MIN) + RAND_OSC_PHASE_MIN;
							//Recalculate index value for the oscillator
							osc1.phaseInc = (osc1.phase / (2.0 * MATH_PI)) * sampleRate;
							break;
						case ParameterID::OSC2_TYPE:
							discreteValue = std::min((int)OscillatorType::NCOUNT - 1, (int)(value * (OscillatorType::NCOUNT)));
							osc2.type = (OscillatorType)discreteValue;
							break;
						case ParameterID::OSC2_AMP:
							osc2.amp = value;
							break;
						case ParameterID::OSC2_FREQ:
							osc2.freq = value * (RAND_OSC_FQ_MAX - RAND_OSC_FQ_MIN) + RAND_OSC_FQ_MIN;
							//Recalculate index value for the oscillator
							osc2.index = osc2.freq * currentOscSample;
							while (osc2.index >= sampleRate) osc2.index -= sampleRate;
							break;
						case ParameterID::OSC2_PHASE:
							osc2.phase = value * (RAND_OSC_PHASE_MAX - RAND_OSC_PHASE_MIN) + RAND_OSC_PHASE_MIN;
							//Recalculate index value for the oscillator
							osc2.phaseInc = (osc2.phase / (2.0 * MATH_PI)) * sampleRate;
							break;
						case ParameterID::MUTE:
							pMute = value > 0.5;
							break;
						}
					}
				}
			}
		}

		//--- Here you have to implement your processing
		for (int i = 0; i < data.numSamples; i += 1) {
			//TODO: Make sure there are inputs when reading and outputs when writing
			if (channel1Input)
			{
				xBuffer[bufferIndex] = data.inputs[0].channelBuffers32[0][i];
			}
			else
			{
				//xBuffer[bufferIndex] = generateNextSample(osc1);
				xBuffer[bufferIndex] = getSample(osc1);
			}

			if (channel2Input)
			{
				if (data.inputs->numChannels == 2)
				{
					yBuffer[bufferIndex] = data.inputs[0].channelBuffers32[1][i];
				}
			}
			else
			{
				//yBuffer[bufferIndex]  = generateNextSample(osc2);
				yBuffer[bufferIndex] = getSample(osc2);
			}

			if (!pMute)
			{
				data.outputs->channelBuffers32[0][i] = xBuffer[bufferIndex];
				if (data.inputs->numChannels == 2 && data.outputs->numChannels == 2)
				{
					data.outputs->channelBuffers32[1][i] = yBuffer[bufferIndex];
				}
			}
			bufferIndex = bufferIndex + 1;
			if (bufferIndex == (int) arraySize) bufferIndex = 0;
			currentOscSample++;
			//need to advance the oscillators even if we dont get the sample
			nextIndex(osc1);
			nextIndex(osc2);
			if (osc1.index <= DBL_EPSILON && osc2.index <= DBL_EPSILON) currentOscSample = 0;
		}

		return kResultOk;
	}


	//------------------------------------------------------------------------
	tresult PLUGIN_API Rand_OscilloscopeProcessor::setupProcessing(Vst::ProcessSetup& newSetup)
	{
		sampleRate = newSetup.sampleRate;
		if (xBuffer) delete[] xBuffer;
		if (yBuffer) delete[] yBuffer;
		if (sinSamples) delete[] sinSamples;
		if (squareSamples) delete[] squareSamples;
		if (sawSamples) delete[] sawSamples;
		if (triangleSamples) delete[] triangleSamples;
		generateOscilloscopeBuffers((int)sampleRate);

		xBuffer = new Steinberg::Vst::Sample32[(int)sampleRate];
		yBuffer = new Steinberg::Vst::Sample32[(int)sampleRate];
		memset(xBuffer, 0, sizeof(Steinberg::Vst::Sample32) * (int)(sampleRate));
		memset(yBuffer, 0, sizeof(Steinberg::Vst::Sample32) * (int)(sampleRate));
		arraySize = 1.0 * sampleRate;
		//XXX: Important to reset this every thime because processsetup may change while the plugin is running 
		// and we may end up with 
		bufferIndex = 0;
		Steinberg::Vst::IMessage* message = allocateMessage();
		message->setMessageID("Setup");
		message->getAttributes()->setInt("ArraySize", (int)sampleRate);
		message->getAttributes()->setInt("BufferIndexPointer", (Steinberg::int64)&bufferIndex);
		//in binary we pass pointer to data we want to send
		//in this case we need the address to the pointer of our buffer
		message->getAttributes()->setBinary("DataPointer", &xBuffer, sizeof(Steinberg::Vst::Sample32*));
		message->getAttributes()->setBinary("yPointer", &yBuffer, sizeof(Steinberg::Vst::Sample32*));
		message->getAttributes()->setFloat("SampleRate", sampleRate);

		//Every nth sample add to an array and send it to the controller
		sendMessage(message);


		//--- called before any processing ----
		return AudioEffect::setupProcessing(newSetup);
	}

	//------------------------------------------------------------------------
	tresult PLUGIN_API Rand_OscilloscopeProcessor::canProcessSampleSize(int32 symbolicSampleSize)
	{
		// by default kSample32 is supported
		if (symbolicSampleSize == Vst::kSample32)
			return kResultTrue;

		// disable the following comment if your processing support kSample64
		/* if (symbolicSampleSize == Vst::kSample64)
			return kResultTrue; */

		return kResultFalse;
	}

	//------------------------------------------------------------------------
	tresult PLUGIN_API Rand_OscilloscopeProcessor::setState(IBStream* state)
	{
		// called when we load a preset, the model has to be reloaded
		IBStreamer streamer(state, kLittleEndian);
		return kResultOk;
	}

	//------------------------------------------------------------------------
	tresult PLUGIN_API Rand_OscilloscopeProcessor::getState(IBStream* state)
	{
		// here we need to save the model
		IBStreamer streamer(state, kLittleEndian);
		return kResultOk;
	}

	//------------------------------------------------------------------------


	Steinberg::tresult Rand_OscilloscopeProcessor::receiveText(const char* text)
	{
		return kResultOk;
	}

	//------------------------------------------------------------------------

	Steinberg::tresult PLUGIN_API Rand_OscilloscopeProcessor::notify(Steinberg::Vst::IMessage* message)
	{
		return kResultOk;
	}

	//------------------------------------------------------------------------

	void Rand_OscilloscopeProcessor::generateSinSamples(double sampleRate)
	{
		sinSamples = new Steinberg::Vst::Sample32[(int)sampleRate];
		for (auto i = 0; i < sampleRate; i++)
		{
			double t = i / sampleRate;
			sinSamples[i] = sin(2 * MATH_PI * 1.0 * t);
		}
	}

	void Rand_OscilloscopeProcessor::generateSawSamples(double sampleRate)
	{
		sawSamples = new Steinberg::Vst::Sample32[(int)sampleRate];
		for (auto i = 0; i < sampleRate; i++)
		{
			double t = i / sampleRate;
			sawSamples[i] = 2 * ((t - floor(t)) - 0.5);
		}
	}

	void Rand_OscilloscopeProcessor::generateTriangeSamples(double sampleRate)
	{
		triangleSamples = new Steinberg::Vst::Sample32[(int)sampleRate];
		for (auto i = 0; i < sampleRate; i++)
		{
			double t = i / sampleRate;
			triangleSamples[i] = 2 * ((t - floor(t)) - 0.5);
			triangleSamples[i] = 2 * (abs(triangleSamples[i]) - 0.5);
		}
	}

	/*
	* TODO: This is unnecessary
	*/
	void Rand_OscilloscopeProcessor::generateSquareSamples(double sampleRate)
	{
		squareSamples = new Steinberg::Vst::Sample32[(int)sampleRate];
		for (auto i = 0; i < sampleRate; i++)
		{
			double t = i / sampleRate;
			squareSamples[i] = (t)-floor(t);
			squareSamples[i] = squareSamples[i] > 0.5 ? 1.0 : -1.0;
		}
	}

	void Rand_OscilloscopeProcessor::generateOscilloscopeBuffers(double sampleRate)
	{
		generateSinSamples(sampleRate);
		generateSawSamples(sampleRate);
		generateSquareSamples(sampleRate);
		generateTriangeSamples(sampleRate);
	}

	Steinberg::Vst::Sample32 Rand_OscilloscopeProcessor::getSample(OscillatorParams& osc)
	{
		int index = osc.index + osc.phaseInc;
		while (index >= (int)arraySize) index -= (int)arraySize;
		switch (osc.type)
		{
		case OscillatorType::SIN:
			return sinSamples[index] * osc.amp;
			break;
		case OscillatorType::SAW:
			return sawSamples[index] * osc.amp;
			break;
		case OscillatorType::TRI:
			return triangleSamples[index] * osc.amp;
			break;
		case OscillatorType::SQ:
			return squareSamples[index] * osc.amp;
			break;
		default:
			return 0.0;
			break;
		}
	}

	//This is better, because it's faster and it removes clipping when the frequency is changed using the knob
	//But now it introduces phase relative the the other oscillator 
	//To combat the phase change (relative to the other oscillator) we recalculate internal index at the frequency change
	//It reintroduces the clipping on the signal, but we dont want them to drift.
	void Rand_OscilloscopeProcessor::nextIndex(OscillatorParams& osc)
	{
		osc.index += ((double)osc.freq);
		while (osc.index >= arraySize) osc.index -= arraySize;
	}

} // namespace RandOhm
