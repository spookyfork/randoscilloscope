#include "../include/ro_animation_target.h"

#include "../include/ro_oscilloscope_view.h"


namespace RandOhm {
	void RandAnimationTarget::animationStart(CView* view, IdStringPtr name)
	{

	}

	void RandAnimationTarget::animationTick(CView* view, IdStringPtr name, float pos)
	{
		if (auto osci = dynamic_cast<RandOhm::Oscilloscope*>(view)) {
			osci->update(pos);
		}
		view->invalid(); //Refresh
	}

	void RandAnimationTarget::animationFinished(CView* view, IdStringPtr name, bool wasCanceled)
	{

	}
}